import React from 'react'
import {BrowserRouter as Router,Route} from 'react-router-dom';
import './App.css';
import MediaItemDetail from './components/MediaItemDetail';
import Search from './components/Search';
import Nav from './components/Nav';
import Login from './components/Login'

export default function App () {
  
  
  
  return (
	<Router>
    <Nav />
    <Route exact path="/" component={Login}></Route>
	<Route exact path="/search" component={Search}></Route>
    <Route path="/mediaitemdetail" component={MediaItemDetail}></Route>
	
	</Router>
  )
    };

