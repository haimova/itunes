import axios from 'axios'

export const login = user => {
    return axios
        .post('user/login', {
            username: user.username,
            password: user.password
        })
        .then(results => {
            sessionStorage.setItem('usertoken', results.data.token)
            sessionStorage.setItem('username', results.data.username)
            return results
        })
        .catch(err => {
            console.log(err)
            return null
        })
}



