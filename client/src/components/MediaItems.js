
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';
import MediaItem from './MediaItem'

class MediaItems extends Component {

    render() {
    
    return (<div className="card-list">
    {
        this.props.searchResult ? this.props.searchResult.map(item =>{
        return <MediaItem item={item} />
        }) : ''
    }
    </div>)}
    
}


export default withRouter(MediaItems)