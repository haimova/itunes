import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

class Nav extends Component {

    logOut(e) {
        e.preventDefault()
        sessionStorage.removeItem('usertoken')
        sessionStorage.removeItem('firstName')
    
        this.props.history.push(`/`)
    }

    login() {
        return (
            <ul className="nav">
                <li>
                    <Link to="/" >
                        Login
                    </Link>
                </li>
                <li>
                    <Link className="disable-link" to="/register" >
                        Register
                    </Link>
                </li>
            </ul>)
    }

    profile() {
        return (
        <ul className="nav">
            <li>
                <Link to="/search">
                    Search
                </Link>
            </li>
            <li>
                <a href="" onClick={this.logOut.bind(this)} >
                    Logout
                </a>
            </li>
        </ul>)
    }

    render() {



        return (
            <nav>
                <div>
                    <h2>Username  {sessionStorage.username ? sessionStorage.username : ''} </h2>
                </div>

                <div>
                    {sessionStorage.usertoken ? this.profile() : this.login()}
                    
                </div>

            </nav>

        )
    }
}

export default withRouter(Nav)