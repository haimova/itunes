import React, { Component } from 'react'
import SearchBar from './SearchBar'
import {getApiUrl} from '../utils'
import MediaItems from './MediaItems'
import axios from 'axios'
import ReactModal from 'react-modal'

class Search extends Component {

    constructor(props) {
        super(props);

        this.state = {
            query: '',
            searchResult: [],
            showModal: false,
            top10: []
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getSearchResult = this.getSearchResult.bind(this);
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    handleOpenModal () {
      this.getTop10Queries()
      this.setState({ showModal: true })
    }
    
    handleCloseModal () {
      this.setState({ showModal: false });
    }

    handleInputChange = (event) => {
        this.setState({
            query: event.target.value
        })
        
    
    }
     handleSubmit = (e) => {
         e.preventDefault()
         this.getSearchResult().then(res => {
            this.setState({
                searchResult: res.results
            })
            
         })
       
     }

     
    
    async getTop10Queries() {
      try {
        const resp = await axios.get(`search/top?user=${sessionStorage.username}`, { headers: { "Authorization": `Bearer ${sessionStorage.getItem('usertoken')}`}})
        this.setState({top10: resp.data})
        return resp.data

      } catch(error) {
        this.setState({ status: 'error in getting top 10' });
        console.log('error' + error)
      }
    }
    async getSearchResult() {
        try {
          this.setState({ status: 'loading' });
          const resp = await axios.get('search?query='+ this.state.query, { headers: { "Authorization": `Bearer ${sessionStorage.getItem('usertoken')}`}})
          this.setState({ status: '' });
          return resp.data
        } catch (e) {
            console.log(e)
            this.setState({ status: 'error in query ' });
        }
    }

 render() {
   return (
    
    <div>
    <SearchBar query={this.state.query} handleInputChange={this.handleInputChange} 
    handleSubmit={this.handleSubmit}/>
    <button onClick={this.handleOpenModal}>Last 10 queries</button>
    <h2>{this.state.status}</h2>
    <MediaItems searchResult={this.state.searchResult} />
    <ReactModal 
           isOpen={this.state.showModal}
           contentLabel="Minimal Modal Example"
        >
          {this.state.status}
          <ul>{this.state.showModal ? this.state.top10.map((item)=> <li >{item.query}</li>) : ""}</ul>
         
          <button onClick={this.handleCloseModal}>Close Modal</button>
    </ReactModal>
    </div>
   )
 }
}

export default Search