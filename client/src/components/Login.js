import React, { Component } from 'react'
import {login} from '../model/user'

class Login extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            message:''
        }
        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    onChange (e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit (e) {
        e.preventDefault()

        const user = {
            username: this.state.username,
            password: this.state.password
        }

        login(user).then(res => {
            if (res) {
                this.props.history.push(`/search`)
            }else{
                this.setState({
                    message: 'User does not exists!'
                });
            }
        })
    }

    render () {
        return (
            
            <div className="container">
                {sessionStorage.usertoken ? this.props.history.push(`/search`) : ""}
                    <div>
                        <form onSubmit={this.onSubmit}>
                            <h1>Please sign in</h1>
                            <div>
                                <label className="login-label">Username</label>
                                <input
                                    name="username"
                                    placeholder="Enter Username"
                                    value={this.state.email}
                                    onChange={this.onChange} />
                            </div>
                            <div>
                                <label className="login-label">Password</label>
                                <input
                                    name="password"
                                    placeholder="Enter Password"
                                    value={this.state.password}
                                    onChange={this.onChange} />
                            </div>
                            <button type="submit">
                                Sign in
                            </button>
                        </form>
                </div>
                {this.state.message !== '' ?
                    <div>
                        {this.state.message}
                    </div>
                    : ''
                }
            </div>
        )
    }
}

export default Login