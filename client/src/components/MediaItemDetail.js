import React, { Component } from 'react'
import ReactPlayer from 'react-player'
import { withRouter } from 'react-router-dom';

class MediaItemDetail extends Component {
    constructor(props) {
        super()
        this.item = props.location.state
        console.log(this.item)
        /*item = {
            previewUrl: props.location.state.previewUrl,
            img: props.location.state.artworkUrl100,
            artistName: props.location.state.artistName,
            collectionName: props.location.state.collectionName,
            country: props.location.state.country,
            type: props.location.state.primaryGenreName,
            trackName: props.location.state.trackName
           
        }*/
    }

    render() {
        return (
            
      <div className="container">
      <div id="media-details-container">
        <div className="media-details-text">
            <img  src={this.item.artworkUrl100} alt={this.item.trackName}/>
            <div className="artist-name">{this.item.artistName}</div>
            <div className="media-name">{this.item.collectionName}</div>
            <div className="country">{this.item.country}</div>
        </div>
        <div className="media-player">
            <ReactPlayer url={this.item.previewUrl} playing controls />
        </div>
      </div>
    </div>
        )
    }
}

export default withRouter(MediaItemDetail)