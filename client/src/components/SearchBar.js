import React, { Component } from 'react'

class Search extends Component {


 render() {
   return (
    
     <form>
    <div className="search">
       <input
         placeholder="Search for..."
         value ={this.props.input}
         onChange={this.props.handleInputChange}
         ref={input => this.search = input}
       />
       <button type="submit" onClick={this.props.handleSubmit}>
           Search
       </button>
       </div>
     </form>
     
   )
 }
}

export default Search