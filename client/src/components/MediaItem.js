
import React from 'react'

import { withRouter } from 'react-router-dom';

const MediaItem = props =>{
    function getItem(item) {
        props.history.push(`/mediaitemdetail`, item)
    }
    return (
       
    <div key={props.item.trackId} className="card" onClick={getItem.bind(this, props.item)}>
            <img src={props.item.artworkUrl100} alt="Artwork" />
            <div className="container">
                <h4><b>{props.item.artistName}</b></h4>
                <p>{props.item.trackName}</p>
            </div>
    </div>)
    
}


export default withRouter(MediaItem)