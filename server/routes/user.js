const jwt = require("jsonwebtoken")
const users_db = require('../db/user');
process.env.SECRET_KEY = 'anyvision';
const router  = require('express').Router();

const login = async (req, res) => {
    const { username, password } = req.body;
    if (!username || !password) {
        return res.status(400).send({'message': 'missing password or username'});
    }
    let isUserExists = await users_db.checkUserExists(username, password)
    if (isUserExists) {
        const payload = {
            username: username,
            password: password
        }
        let token = jwt.sign(payload, process.env.SECRET_KEY, {
            expiresIn: 20000
        })
        payload.token = token;
        return res.send(payload)
    }
    else {
        res.sendStatus(403)
    }
}


// validate auth users 
const validateUser = (req, res, next) => {
    const bearer = req.headers['authorization']
    if (!bearer || bearer.split(' ')[1] === 'null') {
        return res.sendStatus(403)
    }
    const token = bearer.split(' ')[1]
    let user = jwt.verify(token, process.env.SECRET_KEY)
    if (user) {
        req.user = user;
        next()
    } else {
        res.sendStatus(403)
    }
}

router.post("/login", login)

module.exports = router;
module.exports.validateUser = validateUser

