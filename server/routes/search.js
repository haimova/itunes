
const axios = require('axios')
const router  = require('express').Router();
const search_db = require('../db/search');
const {validateUser} = require('./user')
const URL = 'https://itunes.apple.com/search?term='

const getApiUrl = (url, query, limit) => {

    return `${url}${query.split(' ').join('+')}&limit=${limit}`
}

async function search(req, res) {
    try {
        const { query } = req.query;
        let searchResult = await axios.get(getApiUrl(URL,query, 25))
        let saveResult = await search_db.saveSearchedResults(req.user.username, query)
        res.json(searchResult.data)
    } catch(error) {
        console.log(error)
    }
    
}

async function getTop(req, res) {
    const top10Results = await search_db.getTopSearchResults(req.user.username)
    res.json(top10Results)
}

router.get("/top", validateUser, getTop);
router.get("/", validateUser, search);


module.exports = router;