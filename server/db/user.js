let postgresdb = require('./postsqlConnection')

let checkUserExists = async (username, password) => {
    const text = 'SELECT * FROM  anyvision.users WHERE username = $1 AND password = $2'
    const values = [username, password]
    try {
        const res = await postgresdb.query(text, values)
        return res.rows.length == 1
    } catch (err) {
        console.log(err.stack)
        return false
    }
}

module.exports = {
    checkUserExists
} 
//checkUserExists("aviad", "aviad")