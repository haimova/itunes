let postgresdb = require('./postsqlConnection')


let saveSearchedResults = async (user, query) => {
    const text = 'INSERT INTO anyvision.search(username, query, search_date) VALUES($1, $2, to_timestamp($3)) RETURNING *'
    const values = [user, query, Date.now()]
    
    if (user && query) {
        try {
            let date = new Date()
            const res = await postgresdb.query(text, values)
            return res.rows[0]
        } catch (err) {
            console.log(err.stack)
        }
    }
}

let getTopSearchResults = async (user) => {
    const text = 'SELECT query FROM (SELECT DISTINCT ON (query) * FROM anyvision.search WHERE $1 = username ORDER BY query)t  ORDER BY search_date DESC LIMIT 10'
    const values = [user]
    if (user) {
        try {
            let date = new Date()
            const res = await postgresdb.query(text, values)
            return res.rows
        } catch (err) {
            console.log(err.stack)
        }
    }
}

//saveSearchedResults('aviad', 'https://itunes.apple.com/search?term=rihanna')
//getTopSearchResults('aviad')
module.exports = {
    saveSearchedResults,
    getTopSearchResults
}