const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'postgres',
  password: '331144',
  port: 5432,
})

/*pool.query('SELECT * FROM anyvision.users ORDER BY username ASC', (error, results) => {
  if (error) {
    throw error
  }
  console.log(results)
})*/

//create schema
pool.query('CREATE SCHEMA IF NOT EXISTS anyvision AUTHORIZATION postgres'
,(error, results) => {
  if (error) {
    console.log(error)
  }
})

// create tables
pool.query('CREATE TABLE anyvision.search ( \
  username TEXT, \
  query TEXT, \
  search_date DATE, \
  PRIMARY KEY (username, query, search_date))'
,(error, results) => {
  if (!results && error.code != '42P07') {
    console.log(error)
  }
  
})

pool.query('CREATE TABLE anyvision.users ( \
  username TEXT, \
  password TEXT, \
  PRIMARY KEY (username, password))'
,(error, results) => {
  if (!results && error.code != '42P07') {
    console.log(error)
  }
  
})


module.exports =  pool