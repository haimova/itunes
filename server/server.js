'use strict'

const express = require('express');
const bodyParser = require('body-parser');
var timeout = require('connect-timeout')
var app = express();

app.use(timeout(5000))
app.use(haltOnTimedout);

function haltOnTimedout(req, res, next){
  if (!req.timedout) {
      return next()
  }
  res.status(500);
  res.send('500: Internal server error');
}

app.set('port', (process.env.PORT || 8000));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));


app.use('/', express.static('../client/build'))


app.use('/search', require('./routes/search'))
app.use('/user', require('./routes/user'))

let server = app.listen(app.get('port'), function () {
    var host = server.address().address
    var port = server.address().port
    
    console.log("Example app listening at http://%s:%s", host, port)
 })


process.on('uncaughtException', function (error) {
    if (error) {
        console.error("Error  " + error);
    }
});

process.on('unhandledRejection', function (reason, promise) {
    console.error('Unhandled Rejection at:', promise, 'reason:', reason);
});